package ru.epam.multithreading;

public class ChickenThread extends Thread {
    public static void main(String[] args) {
        ChickenThread chickenThread = new ChickenThread();
        Runnable eggRunnable = () -> {
            System.out.println("Поток поклонников яйца работает");
            for (int i = 0; i < 5; i++) {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("яйцо!");
            }
        };
        Thread eggThread = new Thread(eggRunnable);

        chickenThread.start();
        eggThread.start();

        if (chickenThread.isAlive()) {
            try {
                chickenThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (eggThread.isAlive()) {
            try {
                eggThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Первым появилось яйцо!");
        } else {
            System.out.println("Первым появилась курица!");
        }
        System.out.println("Спор закончен!");
    }

}
