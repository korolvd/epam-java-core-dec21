package ru.epam.homework1;

import java.util.Scanner;

public class Coordinates {

    public String defineQuart(float x, float y) {
        String result;
        int normY = y == 0 ? 0 : (int) (y / Math.abs(y));
        int normX = x == 0 ? 0 : (int) (x / Math.abs(x));
        if (normX == 0 && normY == 0) {
            result = "точка О";
        } else if (normX == 0 || normY == 0) {
            result = normX == 0 ? "ось х" : "ось у";
        } else {
            result = switch (normX) {
                case -1 -> normY == -1 ? "III" : "II";
                case 1 -> normY == -1 ? "IV" : "I";
                default -> "something wrong";
            };
        }
        return result;
    }

    public static void main(String[] args) {
        Coordinates coordinates = new Coordinates();
        Scanner in = new Scanner(System.in);
        System.out.print("Введите координату х: ");
        float x = in.nextFloat();
        System.out.print("Введите координату y: ");
        float y = in.nextFloat();
        System.out.println(coordinates.defineQuart(x, y));
    }
}
