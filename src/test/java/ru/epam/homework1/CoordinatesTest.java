package ru.epam.homework1;

import org.junit.Test;

import static org.hamcrest.core.Is.is;

import static org.junit.Assert.*;

public class CoordinatesTest {

    @Test
    public void whenXAndYPositive() {
        Coordinates coordinates = new Coordinates();
        String expect = "I";
        String result = coordinates.defineQuart(1.2f, 4.2f);
        assertThat(expect, is(result));
    }

    @Test
    public void whenXAndYNegative() {
        Coordinates coordinates = new Coordinates();
        String expect = "III";
        String result = coordinates.defineQuart(-1.2f, -4.2f);
        assertThat(expect, is(result));
    }

    @Test
    public void whenXPositiveAndYNegative() {
        Coordinates coordinates = new Coordinates();
        String expect = "IV";
        String result = coordinates.defineQuart(1.2f, -4.2f);
        assertThat(expect, is(result));
    }

    @Test
    public void whenXNegativeAndYPositive() {
        Coordinates coordinates = new Coordinates();
        String expect = "II";
        String result = coordinates.defineQuart(-1.2f, 4.2f);
        assertThat(expect, is(result));
    }

    @Test
    public void whenXAndYZero() {
        Coordinates coordinates = new Coordinates();
        String expect = "точка О";
        String result = coordinates.defineQuart(0, 0);
        assertThat(expect, is(result));
    }

    @Test
    public void whenXZero() {
        Coordinates coordinates = new Coordinates();
        String expect = "ось х";
        String result = coordinates.defineQuart(0, -0.87f);
        assertThat(expect, is(result));
    }

    @Test
    public void whenYZero() {
        Coordinates coordinates = new Coordinates();
        String expect = "ось у";
        String result = coordinates.defineQuart(0.433f, 0);
        assertThat(expect, is(result));
    }

}